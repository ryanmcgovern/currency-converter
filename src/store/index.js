import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sourceCurrency: {},
    targetCurrency: {}
  },
  mutations: {
    SET_SOURCE(state, currency) {
      state.sourceCurrency = currency
    },
    SET_TARGET(state, currency) {
      state.targetCurrency = currency
    }
  },
  actions: {
    // Get source currency
    getSourceCurrency({commit}, {code}) {
      axios.get('http://www.floatrates.com/daily/'+code+'.json', {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        let currency = response.data;
        //commit('SET_SOURCE', {});
        commit('SET_SOURCE', currency);
      })
      .catch((error) => {
        commit('SET_SOURCE', false);
        console.log(error.response.data.message);
      })
    },
    // Get target currency
    getTargetCurrency({commit}, {code}) {
      axios.get('http://www.floatrates.com/daily/'+code+'.json', {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        let currency = response.data;
        //commit('SET_TARGET', {});
        commit('SET_TARGET', currency);
      })
      .catch((error) => {
        commit('SET_TARGET', false);
        console.log(error.response.data.message);
      })
    }
  },
  modules: {
  }
})
